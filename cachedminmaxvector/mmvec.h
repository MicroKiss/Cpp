#ifndef MINMAXVEC__H
#define MINMAXVEC__H

#include <vector>
#include <algorithm>

template <class T>
class MinMaxVector
{
	private:
		std::vector<T> vec;
		T minValue, maxValue;
		bool minValid, maxValid;

	public:

		MinMaxVector()
			: minValid(false), maxValid(false)
		{ }

		const T& min()
		{
			if (!minValid)
			{
				minValue = *(std::min_element(vec.begin(), vec.end()));
				minValid = true;
			}
			return minValue;
		}


		const T& max()
		{
			if (!maxValid)
			{
				maxValue = *(std::max_element(vec.begin(), vec.end()));
				maxValid = true;
			}
			return maxValue;
		}


		const T& max() const
		{
			return maxValid ? maxValue : *(std::max_element(vec.begin(), vec.end()));
		}


		const T& min() const
		{
			return minValid ? minValue : *(std::min_element(vec.begin(), vec.end()));
		}

		MinMaxVector<T>& push_back(const T& t)
		{
			vec.push_back(t);
			minValid = maxValid = false;
			return *this;
		}

		int size() const
		{
			return vec.size();
		}
		
		T& at(int idx)
		{
			minValid = maxValid = false;
			return vec.at(idx);
		}
		
		const T& at(int idx) const
		{
			return vec.at(idx);
		}

		T& operator[](int idx)
		{
			minValid = maxValid = false;
			return vec[idx];
		}

		const T& operator[](int idx) const
		{
			return vec[idx];
		}

		struct iterator: public std::vector<T>::iterator
		{
			bool &minValid, &maxValid;

			iterator(typename std::vector<T>::iterator i, bool &minValid, bool &maxValid)
				: std::vector<T>::iterator(i), minValid(minValid), maxValid(maxValid)
			{ }

			T& operator*()
			{
				minValid = maxValid = false;
				return std::vector<T>::iterator::operator*();
			}
		};

		iterator begin()
		{
			return iterator(vec.begin(), minValid, maxValid);
		}

		iterator end()
		{
			return iterator(vec.end(), minValid, maxValid);
		}
};

#endif