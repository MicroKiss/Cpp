#ifndef SORTED_LIST__
#define SORTED_LIST__
#include <list>
#include <algorithm>

template<typename T, class Compare = std::less<T> >
class SortedList{
private:
    std::list<T> _list;
public:
    void insert(const T &value);
    SortedList(){};
    SortedList(const SortedList &sl);
    int size() const {return _list.size();}
    T front() const {return _list.front();}
    ///3 as
    T back() const {return _list.back();}
    void remove(T value){_list.remove(value);}
    ///4 es
    typename std::list<T>::iterator begin() {return _list.begin();}
    typename std::list<T>::iterator end() {return _list.end();}
    typename std::list<T>::const_iterator begin()const {return _list.cbegin();}
    typename std::list<T>::const_iterator end()const {return _list.cend();}
    ///5 os ????
    template<typename InputIterator>
    SortedList(InputIterator first, InputIterator second){
    _list.clear();
    while (first != second){
        _list.push_back(*first);
        ++first;
    }
    _list.sort(Compare());
    }
};

template<typename T, class Compare >  ///insert
void SortedList<T,Compare>::insert(const T &value){

    typename std::list<T>::iterator it = _list.begin();

    while (Compare()(*it,value) && it != _list.end())
    {
        ++it;
    }

_list.insert(it,value);
}///insert

template<typename T, class Compare> ///copy c-tor
SortedList<T,Compare>::SortedList(const SortedList &sl){
_list = sl._list;
}///copy c-tor


#endif
