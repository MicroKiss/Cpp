#ifndef AECONT_H_INCLUDED
#define AECONT_H_INCLUDED

#include <deque>

template<typename T>
class alternating_endpoint_container
{
public:
    alternating_endpoint_container(){};
    ~alternating_endpoint_container(){};
    void insert(const T &e);
    int size()const {return _deque.size();}
    T& at(const int &ind) { return _deque.at(ind);}
    T at(const int &ind)const { return _deque.at(ind);}
    T& operator[](const int &ind) {return _deque[ind];}
    T operator[](const int &ind)const  {return _deque[ind];}
    void erase();
private:
    bool _l = false;
    std::deque<T> _deque;

public:

    typedef typename std::deque<T>::iterator iterator;
    iterator begin(){return _deque.begin();}
    iterator  end(){return _deque.end();}

    template<typename iter>
    alternating_endpoint_container(iter first,iter second) {
        _deque.clear();
        for (;first!= second ;++first )
        {
            insert(*first);
        }

    }
};

template<typename T>
void alternating_endpoint_container<T>::insert(const T &e){
if (_l){
   _deque.push_back(e);
}
else{
_deque.push_front(e);}
_l = !_l;
}

template<typename T>
void alternating_endpoint_container<T>::erase(){
if(_l){
_deque.pop_back();
}else{
_deque.pop_front();}
_l=!_l;
}

#endif // AECONT_H_INCLUDED
