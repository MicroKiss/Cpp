#ifndef PALALG_H_INCLUDED
#define PALALG_H_INCLUDED
#include <iostream>

template <class InputIt>
bool is_palindrome(InputIt b, InputIt e)
{
    bool l = true;
    int dist = std::distance(b,e)/2;
    e--;
    for(int i = 0; i <= dist && l; i++)
    {
        if(!(*b == *e))
        {
            l = false;
        }
        b++;
        e--;
    }
    return l;
}

template <class InputIt, class F>
bool is_palindrome(InputIt b, InputIt e, F)
{
    bool l = true;
    int dist = std::distance(b,e)/2;
    e--;
    for(int i = 0; i <= dist && l; i++)
    {
        if(!(F()(*b, *e)))
        {
            l = false;
        }
        b++;
        e--;
    }
    return l;
}

template <class InputIt>
void make_palindrome(InputIt b, InputIt e)
{
    e--;
    int dist = std::distance(b,e)/2;
    for(int i = 0; i <= dist; i++)
    {
        *e = *b;
        b++;
        e--;
    }
}

template <class InputIt, class OutputIt>
void copy_palindrome(InputIt b, InputIt e, OutputIt OI, const bool &l)
{
    OI = std::copy(b,e,OI);

    std::reverse_iterator<InputIt>first(e), last(b);

    if(!l)
    {
        ++first;
    }
    std::copy(first,last,OI);
}

template <class InputIt>
void shift_palindrome(InputIt b, InputIt e)
{
    InputIt tmp = b;
    InputIt tmp2 = b;
    InputIt tmp3 = e;
    int dist = 1+((std::distance(b,e)-1)/2);
    std::advance(tmp,std::distance(b,e)/2);
    *b = *tmp;
    e--;
    b++;
    for(int i = 1; i < dist; i++)
    {
        *b = *e;
        e--;
        b++;
    }
    make_palindrome(tmp2, tmp3);
}

#endif // PALALG_H_INCLUDED
