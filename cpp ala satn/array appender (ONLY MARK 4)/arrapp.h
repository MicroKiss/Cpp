#ifndef ARRAPP_H_INCLUDED
#define ARRAPP_H_INCLUDED
#include <vector>

template <typename T>
class array_appender
{
    std::vector<T*> v;
public:
    typedef typename std::vector<T*>::iterator iterator;
    array_appender(T* arr, const int &size)
    {
        for(int i = 0; i< size;i++)
        {
            v.push_back(arr+i); //pointer+integer == pointer + sizeof(*pointer)*i
        }
    }

    void append(T* arr, const int &size)
    {
        for(int i = 0; i<size; i++)
        {
            v.push_back(arr+i);
        }
    }

    void append(std::vector<T> &vec)
    {
        for(int i = 0; i < vec.size(); i++)
        {
            v.push_back(&vec[i]);
        }
    }

    int size() const
    {
        return v.size();
    }

    T& at(const int &index)
    {
        return *(v[index]);
    }

    T& at(const int &index) const
    {
        return *(v[index]);
    }

    T& operator [](const int &index)
    {
        return at(index);
    }

    T& operator [](const int &index) const
    {
        return at(index);
    }

    iterator begin()
    {
        return v.begin();
    }

    iterator end()
    {
        return v.end();
    }
};


#endif // ARRAPP_H_INCLUDED
