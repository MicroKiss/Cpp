#ifndef CHECKSUM_H_INCLUDED
#define CHECKSUM_H_INCLUDED
#include <algorithm>

template <class T, class SumFunc, class Type = typename T::value_type , class Modifier = std::plus<int> >
class checksum
{
    const T& Container;
    int savedsum;
public:
    typedef typename T::iterator T_iterator;
    typedef typename T::const_iterator T_const_iterator;
    checksum(const T& c) : Container(c)
    {
        savedsum = get_checksum();
    }
    int get_checksum() const
    {
        int s = 0;
        for(T_const_iterator i = Container.begin(); i != Container.end(); ++i)
        {
            s += SumFunc()(*i);
        }
        return s;
    }

    bool validate_checksum() const
    {
        return savedsum == get_checksum();
    }

    void update_checksum()
    {
        savedsum = get_checksum();
    }
};


#endif // CHECKSUM_H_INCLUDED
