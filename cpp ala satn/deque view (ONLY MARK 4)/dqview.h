#ifndef DQVIEW_H_INCLUDED
#define DQVIEW_H_INCLUDED
#include <deque>

template <typename T>
class deques_view
{
    std::deque<T*> deq;
public:
    typedef typename std::deque<T>::iterator it;
    typedef typename std::deque<T>::const_iterator const_it;
    typedef typename std::deque<T*>::iterator it2;

    deques_view(std::deque<T> &src)
    {
        for(it i = src.begin(); i != src.end(); ++i)
        {
            deq.push_back(&(*i));
        }
    }
    void add(std::deque<T> &src)
    {
        for(it i = src.begin();i != src.end();++i)
        {
            deq.push_back(&(*i));
        }
    }

    T& at(const int &i)
    {
        return *(deq.at(i));
    }

    T at(const int &i) const
    {
        return *(deq.at(i));
    }
    T& operator [](const int &i)
    {
        return at(i);
    }
    T operator [](const int &i) const
    {
        return at(i);
    }

    int size() const
    {
        return deq.size();
    }

    void sort()
    {
        sort(std::less<T>());
    }
    template <typename F>
    void sort(F f)
    {
        bool sorted = false;
        while(!sorted)
        {
            sorted = true;
            it2 i = deq.begin();
            i++;
            while(i < deq.end())
            {
                if(f(*(*i),*(*(i-1))))
                {
                    T tmp = *(*(i-1));
                    *(*(i-1)) = *(*i);
                    *(*i) = tmp;
                    sorted = false;
                }
                i++;
            }
        }
    }
};

#endif // DQVIEW_H_INCLUDED
