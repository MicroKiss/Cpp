#ifndef ARFILTVIEW_H_INCLUDED
#define ARFILTVIEW_H_INCLUDED
#include <vector>

template <typename T, typename Filter>
class array_filter_view
{
    T* arr;
    std::vector<T> Vals;
    int filtsize;
public:
    static int const npos = -1;
    typedef T* iterator;
    typedef const T* const_iterator;
    array_filter_view(T* p, const int &oarrsize)
    {
        arr = p;
        filtsize = 0;
        for(int i = 0; i<oarrsize;i++)
        {
            Vals.push_back(p[i]);
            if(Filter()(p[i]))
            {
                arr[filtsize] = p[i];
                filtsize++;
            }
        }
    }

    ~array_filter_view()
    {
        for(int i = 0; i<Vals.size();i++)
        {
            arr[i] = Vals[i];
        }
    }

    int size() const
    {
        return filtsize;
    }

    int index_of(const T &val) const
    {
        for(int i = 0; i < filtsize;i++)
        {
            if(arr[i] == val)
            {
                return i;
            }
        }
        return npos;
    }

    template <typename Filterer>
    int index_if(Filterer f) const
    {
        for(int i = 0; i < Vals.size();i++)
        {
            if(f(Vals[i]) != Filter()(Vals[i]))
            {
                return npos;
            }
        }
        return 0;
    }

    T* begin()
    {
        return arr;
    }

    T* end()
    {
        return arr+filtsize;
    }

    T* begin() const
    {
        return arr;
    }

    T* end() const
    {
        return arr+filtsize;
    }
};

#endif // ARFILTVIEW_H_INCLUDED
