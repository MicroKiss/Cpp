#ifndef SETFILTER_H_INCLUDED
#define SETFILTER_H_INCLUDED
#include <set>
#include <algorithm>
#include <iostream>

template <class T, class Compare = std::less<T> >
class set_filtering
{
public:
    typedef typename std::set<T>::iterator iterator;
    set_filtering(std::set<T, Compare> &s)
    {
        setPointer = &s;
    }

    void filter(const T Key)
    {
        iterator it = setPointer->find(Key);
        if(it != setPointer->end())
        {
            FilteredElements.insert(*it);
            setPointer->erase(it);
        }
    }

    void unfilter(const T Key)
    {
        iterator it = FilteredElements.find(Key);
        if(it != FilteredElements.end())
        {
            setPointer->insert(Key);
            FilteredElements.erase(it);
        }
    }

    void inverse()
    {
        setPointer->swap(FilteredElements);
    }

    ~set_filtering()
    {
        setPointer->insert(FilteredElements.begin(),FilteredElements.end());
    }

    void operator~()
    {
        inverse();
    }

    template <class Pred>
    void operator+=(Pred P)
    {
        iterator it = setPointer->begin();
        while(it != setPointer->end())
        {
            T key = *it;
            it++;
            if(P(key))
            {
                filter(key);
            }
        }
    }

    template <class Pred>
    void operator-=(Pred P)
    {
        iterator it = FilteredElements.begin();
        while(it != FilteredElements.end())
        {
            T key = *it;
            it++;
            if(P(key))
            {
                unfilter(key);
            }
        }
    }


private:
    std::set<T, Compare>* setPointer;
    std::set<T, Compare> FilteredElements;
};


#endif // SETFILTER_H_INCLUDED
