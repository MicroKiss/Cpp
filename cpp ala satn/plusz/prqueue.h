#ifndef PRIORITY_QUEUE_H_INCLUDED
#define PRIORITY_QUEUE_H_INCLUDED
#include <map>
#include <queue>

template <typename T, typename T2, class Compare = std::less<T> >
class priority_queue
{
    typedef typename std::multimap<T,T2,Compare>::iterator iterator;
public:
    priority_queue()
    {

    }

    priority_queue(std::priority_queue<T> pq)
    {
        while(!pq.empty())
        {
            push(pq.top(),pq.top());
            pq.pop();
        }
    }

    template <class Container, class Compare2 = std::greater<T> >
    priority_queue(std::priority_queue<T,Container,Compare2> pq)
    {
        while(!pq.empty())
        {
            push(pq.top(),pq.top());
            pq.pop();
        }
    }

    void push(const T Key, const T2 Val)
    {
        std::pair<T,T2> p;
        p.first = Key;
        p.second = Val;
        Container.insert(p);
    }

    void update_priority(const T Key, const T2 Val, const T ModifiedKey)
    {
        std::pair<iterator,iterator> p;
        p = Container.equal_range(Key);
        iterator it = p.first;
        while(it != p.second && it->second != Val)
        {
            it++;
        }
        if(it != p.second)
        {
            Container.erase(it);
            push(ModifiedKey,Val);
        }
    }

    T2& top()
    {
        return Container.rbegin()->second;
    }

    T2 top() const
    {
        return Container.rbegin()->second;
    }

    void pop()
    {
        Container.erase(--(Container.rbegin().base()));
    }

    int size() const
    {
        return Container.size();
    }
private:
    std::multimap<T,T2,Compare> Container;
};

#endif // PRIORITY_QUEUE_H_INCLUDED
