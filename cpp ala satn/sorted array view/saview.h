#ifndef SAVIEW_H_INCLUDED
#define SAVIEW_H_INCLUDED
#include <algorithm>
#include <vector>
#include <list>

template <class T, class Comparer = std::less<T> >
class sorted_array_view
{
private:
    int S;
    T* Tarray;
    std::vector<T> Ovals;
public:
    static const int npos;
    sorted_array_view(T* p, int s)
    {
        bool sorted = true;
        S = s;
        Tarray = p;
        for(int i = 0; i<S;++i)
        {
            Ovals.push_back( Tarray[i] );
        }
        do
        {
            sorted = true;
            for(int i = 1; i<S;++i)
            {
                if(Comparer()(Tarray[i], Tarray[i-1]))
                {
                    T tmp = Tarray[i-1];
                    Tarray[i-1] = Tarray[i];
                    Tarray[i] = tmp;
                    sorted=false;
                }
            }
        }while(!sorted);
    }

    ~sorted_array_view()
    {
        for(int i = 0; i<S;++i)
        {
            Tarray[i] = Ovals[i];
        }
    }

    int count(const T &val) const
    {
        const std::pair<T*, T*>& range = std::equal_range(Tarray, Tarray + S, val, Comparer());
        return range.second - range.first;
    }

    int index_of(const T &val) const
    {
        const std::pair<T*, T*>& range = std::equal_range(Tarray, Tarray + S, val, Comparer());
        if(range.first != range.second)
        {
        return range.first - Tarray;
        }
        return npos;
    }

    int size() const
    {
        return S;
    }

    template <typename Cont>
    void copy(Cont &c) const
    {
        c.clear();
        std::copy(Tarray, Tarray+S, std::back_inserter(c));
    }

    const T* begin() const
    {
        return Tarray;
    }

    const T* end() const
    {
        return Tarray+S;
    }
};

template<class T, class Comparer>
const int sorted_array_view<T, Comparer>::npos = -1;

#endif // SAVIEW_H_INCLUDED
