#ifndef FILTMMAP_H_INCLUDED
#define FILTMMAP_H_INCLUDED
#include <map>

template <typename Key, typename Type, typename KeyFilter, typename ValFilter, typename Compare = std::less<Key> >
class filterable_multimap
{
    std::multimap<Key, Type, Compare> Container;
    std::multimap<Key, Type, Compare> FilteredCont;
    bool iskeyfilteron;
    bool isvalfilteron;
public:
    typedef typename std::multimap<Key,Type,Compare>::iterator iterator;
    typedef typename std::multimap<Key,Type,Compare>::const_iterator const_iterator;
    filterable_multimap()
    {
        iskeyfilteron = false;
        isvalfilteron = false;
    }

    void insert(const Key &k, const Type &t)
    {
        std::pair<Key, Type> p(k,t);
        Container.insert(p);
    }

    void set_value_filter_on()
    {
        isvalfilteron = true;
    }

    void set_value_filter_off()
    {
        isvalfilteron = false;
    }

    void set_key_filter_on()
    {
        iskeyfilteron = true;
    }

    void set_key_filter_off()
    {
        iskeyfilteron = false;
    }

    void UpdateFilteredContainer()
    {
        FilteredCont.clear();
        if(iskeyfilteron)
        {
            if(isvalfilteron)
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!ValFilter()((*i).second) && !KeyFilter()((*i).first))
                    {
                        FilteredCont.insert(*i);
                    }
                }
            }
            else
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!KeyFilter()((*i).first))
                    {
                        FilteredCont.insert(*i);
                    }
                }
            }
        }
        else
        {
            if(isvalfilteron)
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!ValFilter()((*i).second))
                    {
                        FilteredCont.insert(*i);
                    }
                }
            }
            else
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    FilteredCont.insert(*i);
                }
            }
        }
    }

    int size() const
    {
        int c = 0;
        if(isvalfilteron)
        {
            if(iskeyfilteron)
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!ValFilter()((*i).second) && !KeyFilter()((*i).first))
                    {
                        c++;
                    }
                }
            }
            else
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!ValFilter()((*i).second))
                    {
                        c++;
                    }
                }
            }
        }
        else
        {
            if(iskeyfilteron)
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!KeyFilter()((*i).first))
                    {
                        c++;
                    }
                }
            }
            else
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    c++;
                }
            }
        }
        return c;
    }

    int count(const Key &k) const
    {
        int c = 0;
        if(isvalfilteron)
        {
            if(iskeyfilteron)
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!ValFilter()((*i).second) && (*i).first == k && !KeyFilter()((*i).first))
                    {
                        c++;
                    }
                }
            }
            else
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if(!ValFilter()((*i).second) && (*i).first == k)
                    {
                        c++;
                    }
                }
            }
        }
        else
        {
            if(iskeyfilteron)
            {
                for(const_iterator i = Container.begin(); i!=Container.end();i++)
                {
                    if((*i).first == k && !KeyFilter()((*i).first))
                    {
                        c++;
                    }
                }
            }
            else
            {
                c = Container.count(k);
            }
        }
        return c;
    }

    Type& operator ()(const Key &k, int ind)
    {
        for(iterator i = Container.begin(); i != Container.end(); i++)
        {
            if((*i).first == k && ind == 0)
            {
                return (*i).second;
            }
            else if((*i).first == k)
            {
                ind--;
            }
        }
    }

    Type operator ()(const Key &k, int ind) const
    {
        for(const_iterator i = Container.begin(); i != Container.end(); i++)
        {
            if((*i).first == k && ind == 0)
            {
                return (*i).second;
            }
            else if((*i).first == k)
            {
                ind--;
            }
        }
    }

    iterator begin()
    {
        UpdateFilteredContainer();
        return FilteredCont.begin();
    }

    iterator end()
    {
        UpdateFilteredContainer();
        return FilteredCont.end();
    }

    const_iterator begin() const
    {
        return FilteredCont.begin();
    }

    const_iterator end() const
    {
        return FilteredCont.end();
    }
};

#endif // FILTMMAP_H_INCLUDED
