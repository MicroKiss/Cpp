#ifndef OEMSET_H_INCLUDED
#define OEMSET_H_INCLUDED
#include <set>
#include <algorithm>

template <class T, class Compare = std::less<T>, class Equality = std::equal_to<T> >
class optional_equality_multiset
{
    typedef typename std::multiset<T>::iterator IT;
    bool eq = false;
    std::multiset<T, Compare> Tset;
    struct equ : std::binary_function <T,T,bool>
    {
        Equality equality;
        bool operator()(const T& lhs, const T& rhs) const
        {
            return equality(lhs,rhs);
        }
    };
public:
    typedef typename std::multiset<T>::const_iterator const_iterator;
    typedef typename std::equal_to<T> eqr;
    void insert(const T &a)
    {
        Tset.insert(a);
    }
    void use_equality()
    {
        eq = true;
    }
    void use_equivalence()
    {
        eq = false;
    }
    std::multiset<T> get()
    {
        return Tset;
    }
    int count(const T &a) const
    {
        if(eq)
        {
            int c = 0;
            for(IT i = Tset.begin(); i != Tset.end();++i)
            {
                if(Equality()(*i,a))
                {
                    c++;
                }
            }
            return c;
        }
        else
        {
            return Tset.count(a);
        }
    }

    void erase(const T &a)
    {
        if(eq)
        {
            for(IT i = Tset.begin(); i!= Tset.end();++i)
            {
                if(Equality()(*i,a))
                {
                    Tset.erase(i);
                }
            }
        }
        else
        {
            Tset.erase(a);
        }
    }
    const_iterator find(const T &a) const
    {
        if(eq)
        {
            for(const_iterator i = Tset.begin(); i!= Tset.end();++i)
            {
                if(Equality()(*i,a))
                {
                    return i;
                }
            }
        }
        else
        {
            return Tset.find(a);
        }
        return Tset.end();
    }

    /*IT begin()
    {
        return Tset.begin();
    }

    IT end()
    {
        return Tset.end();
    }*/
    const_iterator begin() const
    {
        return Tset.begin();
    }

    const_iterator end() const
    {
        return Tset.end();
    }
    template<class Cmp>
    operator std::multiset<T, Cmp>() const {
        return std::multiset<T, Cmp>(Tset.begin(), Tset.end());
    }
};

#endif // OEMSET_H_INCLUDED
