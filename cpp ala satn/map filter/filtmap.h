#ifndef FILTMAP_H_INCLUDED
#define FILTMAP_H_INCLUDED
#include <map>

template <typename Key, typename Type, typename Filter, typename Compare = std::less<Key> >
class filterable_map
{
    std::map<Key,Type, Compare> Container;
    bool isfilteron;
public:
    typedef typename std::map<Key,Type>::iterator iterator;
    typedef typename std::map<Key,Type>::const_iterator const_iterator;
    filterable_map()
    {
        isfilteron = false;
    }
    void insert (const Key &k,const Type &t)
    {
        std::pair<Key, Type> p(k,t);
        Container.insert(p);
    }
    void set_filter_on()
    {
        isfilteron = true;
    }
    void set_filter_off()
    {
        isfilteron = false;
    }

    int size() const
    {
        int c = 0;
        if (isfilteron)
        {
            for(const_iterator i = Container.begin(); i!= Container.end();i++)
            {
                if(!(Filter()((*i).first)))
                {
                    c++;
                }
            }
        }
        else
        {
            for(const_iterator i = Container.begin(); i!= Container.end();i++)
            {
                    c++;
            }
        }
        return c;
    }

    int count(const Key &val) const
    {
        int c = 0;
        if(isfilteron)
        {
            for(const_iterator i = Container.begin(); i!= Container.end();i++)
            {
                if(!(Filter()((*i).first)) && (*i).first == val)
                {
                    c++;
                }
            }
        }
        else
        {
            c = Container.count(val);
        }
        return c;
    }

    Type& at(const Key &k)
    {
        for(iterator i = Container.begin(); i!= Container.end();i++)
            {
                if(!Compare()((*i).first,k) && !Compare()(k,(*i).first))
                {
                    return (*i).second;
                }
            }
    }

    Type at(const Key &k) const
    {
        for(const_iterator i = Container.begin(); i!= Container.end();i++)
            {
                if(!Compare()((*i).first,k) && !Compare()(k,(*i).first))
                {
                    return (*i).second;
                }
            }
    }

    operator std::map<Key, Type, Compare>() const
    {
        if(isfilteron)
        {
            std::map<Key, Type, Compare> cpy;
            for(const_iterator i = Container.begin(); i!= Container.end();i++)
            {
                if(!(Filter()((*i).first)))
                {
                    cpy.insert(*i);
                }
            }
            return cpy;
        }
        else
        {
            return Container;
        }
    }
};




#endif // FILTMAP_H_INCLUDED
