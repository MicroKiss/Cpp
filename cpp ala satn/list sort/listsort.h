#ifndef LISTSORT_H_INCLUDED
#define LISTSORT_H_INCLUDED
#include <list>

template <typename T, class Compare = std::less<T> >
class list_sorter
{
    std::list<T>* ListCont;
    int list_c;
public:
    typedef typename std::list<T>::iterator literator;
    list_sorter (std::list<T> &l)
    {
        l.sort(Compare());
        list_c = 1;
        ListCont = &l;
    }

    std::list<T>* asd()
    {
        return ListCont;
    }

    list_sorter& add(std::list<T> &l)
    {
        list_c = list_c + 1;
        for(literator i = l.begin(); i != l.end(); ++i)
        {
            (*ListCont).push_back(*i);
        }
        l.clear();
        ListCont->sort(Compare());
        return (*this);
    }

    list_sorter& remove(const T &val)
    {
        literator b; literator e;
        for(literator i = ListCont->begin(); i != ListCont->end();++i)
        {
            b = i;
            e = b;
            if(!Compare()(*i,val) && !Compare()(val,*i))
            {
                e++;
                while(!Compare()(*e,val) && !Compare()(val,*e) && e != ListCont->end())
                {
                    std::cout << *e << " ";
                    ++e;
                }
                break;
            }
        }
        ListCont->erase(b,e);
        ListCont->sort(Compare());
        return (*this);
    }

    T front()
    {
        return ListCont->front();
    }

    int list_count() const
    {
        return list_c;
    }

    void operator+=(list_sorter<T> &rhs)
    {
        this->add(*(rhs.asd()));
    }
};

#endif // LISTSORT_H_INCLUDED
