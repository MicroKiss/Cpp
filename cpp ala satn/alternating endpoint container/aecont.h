#ifndef AECONT_H_INCLUDED
#define AECONT_H_INCLUDED
#include <deque>

template <typename T>
class alternating_endpoint_container
{
    std::deque<T> Container;
    bool is_front;
public:
    typedef typename std::deque<T>::iterator iterator;
    typedef typename std::deque<T>::iterator const_iterator;
    alternating_endpoint_container() : is_front(true) {}

    template <typename T2>
    alternating_endpoint_container(T2 b, T2 e) :is_front(true)
    {
        for(;b!=e;b++)
        {
            insert(*b);
        }
    }

    void insert(const T &val)
    {
        if(is_front)
        {
            Container.push_front(val);
        }
        else
        {
            Container.push_back(val);
        }
        is_front = !is_front;
    }

    int size() const
    {
        return Container.size();
    }

    T& at(const int &index)
    {
        return Container.at(index);
    }

    T at(const int &index) const
    {
        return Container.at(index);
    }

    void erase()
    {
        if(is_front)
        {
            Container.pop_front();
        }
        else
        {
            Container.pop_back();
        }
        is_front = !is_front;
    }

    T& operator[](const int &index)
    {
        return Container[index];
    }

    T operator[](const int &index) const
    {
        return Container[index];
    }

    iterator begin()
    {
        return Container.begin();
    }
    iterator end()
    {
        return Container.end();
    }
};

#endif // AECONT_H_INCLUDED
